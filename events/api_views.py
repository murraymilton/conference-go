from django.http import JsonResponse, Http404
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from .acls import get_photo, get_weather_data


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]
    def get_extra_data(self, o):
        return { "state": o.state.abbreviation}

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]

class ConferenceDetailEncoder(LocationListEncoder,ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):


   if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
            )
   else:
        try:
            content = json.loads(request.body)
        except JSONDecodeError:
            return JsonResponse(
                {"error": "malformed request"}
            )
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"})
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):

    if request.method == "GET":
            conference = Conference.objects.get(id=id)
            weather = get_weather_data(
            conference.location.city, conference.location.state.abbreviation
            )
            return JsonResponse(
                {"conference": conference, "weather":weather},
                encoder=ConferenceDetailEncoder,
                safe=False
             )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        if "location" in content:
            try:
                location = Location.objects.get(location=content["location"])
                content["location"] = location
            except Location.DoesNotExist:
                return JsonResponse({"message": "Location invalid could not be found"})
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)

        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse({"locations": locations}, encoder=LocationListEncoder)

    else:   
        try:
            content = json.loads(request.body)
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abb"},
                status=400,
            )
        photo = get_photo(content["state"].abbreviation, content["city"])
        #Undate the photo after the creation of the post 
        content.update(photo)





        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )







@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):

    if request.method == "GET":
        try:
            location = Location.objects.get(id=id)
            if not location.picture_url:
                photo = get_photo(location.city, location.state.abbreviation)
                locatoin.picture_url = photo.get("picture_url")
                location.save()
            return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)
        except Location.DoesNotExist:
            raise Http404("Location not found")
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


    else:
        content = json.loads(request.body)

        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abb"},
                status=404,
            )
    Location.objects.filter(id=id).update(**counter)
    location = Location.objects.get(id=id)
    return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
 