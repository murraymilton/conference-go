import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
#  one that makes a request to use the Pexels API, and one 
#  to make a request to the Open Weather API. Then, 
#  use those functions in your views.

def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query":city + " " + state} 
    res = requests.get(url,params=params, headers=headers)
    content = json.loads(res.content)
    content_photos = content["photos"]
    #content   clue = json.loads(response.content)



    try:
        return {"picture_url": content_photos[0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

#create a function to set the lat and lon within the params



def get_weather_data(city, state):
    ISO = str(3166 - 2)

    params_geo ={
        "q": f"{city}, {state}, {ISO}",
        "appid": OPEN_WEATHER_API_KEY,
    }
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct"
    res =  requests.get(geo_url, params=params_geo)
    content = json.loads(res.content)
    try:
        lat = content[0].get("lat")
        lon = content[0].get("lon")
    except IndexError:
            return None

    params_weather = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units":"imperial",
    }
    weather_url = f"https://api.openweathermap.org/data/2.5/weather"
    res_weather = requests.get(weather_url, params=params_weather)
    content_weather = json.loads(res_weather.content)
    try:
        return {
            "weather": {
                "description": content_weather["weather"][0]["description"],
                "temp": content_weather["main"]["temp"],
            }
        }
    except:
        return {
            "weather": None,
        }
       
  

    
