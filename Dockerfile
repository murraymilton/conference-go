FROM python:3
#Loads base image from docker hub

ENV PYTHONUNBUFFERED 1
#Creates an environmental variable

WORKDIR /app
#Creates a working directory

COPY accounts accounts
COPY attendees attendees
COPY common common
COPY conference_go conference_go
COPY events events
COPY presentations presentations
COPY requirements.txt requirements.txt
COPY manage.py manage.py
#Copies file from local machine to image/container

RUN pip install -r requirements.txt
#Runs a command on our container

CMD gunicorn --bind 0.0.0.0:8000 conference_go.wsgi
#Starts our server
